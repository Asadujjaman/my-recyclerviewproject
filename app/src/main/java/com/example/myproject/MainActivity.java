package com.example.myproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class MainActivity extends AppCompatActivity {
    private ArrayList<ExampleItem> mExampleList;

    private RecyclerView mRecyclerView;
    private ExampleAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Button insertButton;
    private Button removeButton;
    private EditText insertEdt;
    private EditText removeEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createExampleList();
        buildRecyclerView();
        setButtons();

        insertButton =(Button)findViewById(R.id.insert_button_id);
        removeButton = (Button)findViewById(R.id.delete_button_id);
        insertEdt = (EditText)findViewById(R.id.edt_text_insert);
        removeEdt = (EditText)findViewById(R.id.edt_text_delete);
        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = Integer.parseInt(insertEdt.getText().toString());
                insertItem(position);
            }
        });
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = Integer.parseInt(removeEdt.getText().toString());
                removeItem(position);
            }
        });
    }
    public void insertItem(int position){
        mExampleList.add(position,new ExampleItem(R.drawable.ic_android,"New item at Position"+position, "This is line 2"));
        mAdapter.notifyItemInserted(position);
    }
    public void removeItem(int position){
        mExampleList.remove(position);
        mAdapter.notifyItemRemoved(position);
    }
    public void changeItem(int position,String text){
        mExampleList.get(position).changeText1(text);
        mAdapter.notifyItemChanged(position);
    }
    public void createExampleList(){
        mExampleList = new ArrayList<>();
        mExampleList.add(new ExampleItem(R.drawable.ic_android,"Line 1","Line 2"));
        mExampleList.add(new ExampleItem(R.drawable.ic_adb,"Line 3","Line 4"));
        mExampleList.add(new ExampleItem(R.drawable.ic_account,"Line 5","Line 6"));

        /*exampleItems.add(new ExampleItem(R.drawable.ic_android,"Line 7","Line 8"));
        exampleItems.add(new ExampleItem(R.drawable.ic_adb,"Line 9","Line 10"));
        exampleItems.add(new ExampleItem(R.drawable.ic_account,"Line 11","Line 12"));
        exampleItems.add(new ExampleItem(R.drawable.ic_android,"Line 13","Line 14"));
        exampleItems.add(new ExampleItem(R.drawable.ic_adb,"Line 15","Line 16"));
        exampleItems.add(new ExampleItem(R.drawable.ic_account,"Line 17","Line 18"));
        exampleItems.add(new ExampleItem(R.drawable.ic_android,"Line 19","Line 20"));
        exampleItems.add(new ExampleItem(R.drawable.ic_adb,"Line 21","Line 22"));
        exampleItems.add(new ExampleItem(R.drawable.ic_account,"Line 23","Line 24"));
        exampleItems.add(new ExampleItem(R.drawable.ic_android,"Line 25","Line 26"));
        exampleItems.add(new ExampleItem(R.drawable.ic_adb,"Line 27","Line 28"));
        exampleItems.add(new ExampleItem(R.drawable.ic_account,"Line 29","Line 30"));
        exampleItems.add(new ExampleItem(R.drawable.ic_android,"Line 31","Line 32"));
        exampleItems.add(new ExampleItem(R.drawable.ic_adb,"Line 33","Line 34"));
        exampleItems.add(new ExampleItem(R.drawable.ic_account,"Line 35","Line 36"));
        exampleItems.add(new ExampleItem(R.drawable.ic_android,"Line 37","Line 38"));
        exampleItems.add(new ExampleItem(R.drawable.ic_adb,"Line 39","Line 40"));
        exampleItems.add(new ExampleItem(R.drawable.ic_account,"Line 41","Line 42"));*/
    }
    public void buildRecyclerView(){
        mRecyclerView = findViewById(R.id.recycler_View_id);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ExampleAdapter(mExampleList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new ExampleAdapter.OnItemClickListener() {
            @Override
            public void onItemClicke(int position) {
                changeItem(position,"Clicked");
            }

            @Override
            public void onDeleteClick(int position) {
                removeItem(position);
            }
        });
    }
    public void setButtons(){
        insertButton =(Button)findViewById(R.id.insert_button_id);
        removeButton = (Button)findViewById(R.id.delete_button_id);
        insertEdt = (EditText)findViewById(R.id.edt_text_insert);
        removeEdt = (EditText)findViewById(R.id.edt_text_delete);
        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = Integer.parseInt(insertEdt.getText().toString());
                insertItem(position);
            }
        });
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = Integer.parseInt(removeEdt.getText().toString());
                removeItem(position);
            }
        });

    }
}
